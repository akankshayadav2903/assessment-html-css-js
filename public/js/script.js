var products=[];
var saveforLater=[];

function getProducts() {
    
    return fetch("http://localhost:3000/products",
    {
        method:"GET"
    }).then((res) => 
    {
        if(res.ok) {
            return res.json();
        }
        else {
            return Promise.reject(res.status)
        }
    }).then((x) => {
        products = x
        showProducts(products)
        return x
    })

}


let showProducts = (x) => {

    x.forEach(element => {
        document.getElementById("productslist").innerHTML+= 
    `<div class="divProducts">
    <img src=${element.thumbnail} alt="${element.brand}"><br><br>
    <li><h5>${element.title}</h5></li>
    <li><h6>${element.description}</h6></li>
    <li><h6>${element.price}</h6></li>
    <li><h6>${element.rating}</h6></li>
    <li><h6>${element.stock}</h6></li>
    <li><h6>${element.category}</h6></li>
    <button type="button" class="btn btn-primary" id="but" onclick="addForLater(${element.id})">Save For Later</button>
    </div><br>
    `
})
}

function getSaveForLater() {

 return fetch("http://localhost:3000/saveforLater",
 {
    method:"GET"
 }).then((res) =>
 {
    if(res.ok) {
       return res.json()
    }
    else {
            return Promise.reject(res.status)
    }
 }).then((x) => {
    saveforLater = x
    showSavedforLater(x)
    return x
})
}




function addForLater(id) {
    let obj=products.find(x =>
        {
            if(x.id == id) {
                return x
            }
        })

let save=saveforLater.find(y => {
    if(y.id == obj.id) {
        return y
    }
})

if(save) {
    return Promise.reject(new Error("Product is already added to save for later."));
}
else {
    return fetch("http://localhost:3000/saveforLater",
    {
        method:"POST",
        headers: {'content-type' : 'application/json'},
        body: JSON.stringify(obj)
    }).then(res => {
        if(res.ok) {
            return res.json();
        }
    }).then(x => {
        saveforLater.push(x);
        showSavedforLater(saveforLater);
        return saveforLater;
    }
    )
}
}

let showSavedforLater = (x) => {
    x.forEach(element => {
        document.getElementById("savelist").innerHTML+= 
        `<div class="divProducts">
        <img src=${element.thumbnail} alt="${element.brand}"><br>
    <li><h5>${element.title}</h5></li>
    <li><h6>${element.description}</h6></li>
    <li><h6>${element.price}</h6></li>
    <li><h6>${element.rating}</h6></li>
    <li><h6>${element.stock}</h6></li>
    <li><h6>${element.category}</h6></li>
    </div><br>
    `
    })
}